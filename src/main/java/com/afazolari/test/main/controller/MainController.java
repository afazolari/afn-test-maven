package com.afazolari.test.main.controller;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@RequestMapping(method = GET, path = "/")
	@ResponseStatus(OK)
	public String get() {
		return "Teste";
	}

}
